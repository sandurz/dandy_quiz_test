import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'breedSearch'
})
export class BreedSearchPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const breedOptions = value;
    const searchQuery = args[0];

    if (!breedOptions || !breedOptions.length) {
      return [];
    } if (!searchQuery) {
      return breedOptions;
    }

    const matches = [];

    for (const option of breedOptions) {
      if (option.name.toLowerCase().includes(searchQuery.toLowerCase())) {
        matches.push(option);
      }
    }

    return matches;
  }

}
