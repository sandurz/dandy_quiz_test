import { BreedSearchPipe } from './breed-search.pipe';

import { SINGLE_BREEDS, MIXED_BREEDS } from '../components/quiz/quiz.data';

interface BreedOption {
  type: 'single' | 'mixed' | 'special';
  name: string;
  id: string;
}



describe('BreedSearchPipe', () => {
  let breedOptions = [];

  prepareBreedOptions();

  it('create an instance', () => {
    const pipe = new BreedSearchPipe();
    expect(pipe).toBeTruthy();
  });

  it('should be case insensitive', () => {
    const pipe = new BreedSearchPipe();
    const lowercaseResults = pipe.transform(breedOptions, 'hound');
    const uppercaseResults = pipe.transform(breedOptions, 'Hound');
    expect(lowercaseResults).toBeTruthy();
    expect(uppercaseResults).toBeTruthy();
    expect(lowercaseResults.length).toEqual(uppercaseResults.length);
  });

  it('should not have Other and Unknown options when there are matches', () => {
    const pipe = new BreedSearchPipe();
    const results = pipe.transform(breedOptions, 'Staffordshire Bull Terrier');
    expect(results).toBeTruthy();
    expect(results.length).toBe(1);
  });

  function prepareBreedOptions() {
    for (const breed of SINGLE_BREEDS) {
      breedOptions.push({
        type: 'single',
        name: breed,
        id: getBreedIdFromName(breed)
      });
    }

    for (const breed of MIXED_BREEDS) {
      breedOptions.push({
        type: 'mixed',
        name: breed,
        id: getBreedIdFromName(breed)
      });
    }

    breedOptions = breedOptions.sort((a, b) => a.name.localeCompare(b.name));

    breedOptions.push({
      type: 'special',
      name: 'Other',
      id: 'other'
    });
    breedOptions.push({
      type: 'special',
      name: 'Unknown',
      id: 'unknown'
    });
  }

  function getBreedIdFromName(name: string) {
    return name.toLowerCase().replace(/\s+/g, '-');
  }

});

