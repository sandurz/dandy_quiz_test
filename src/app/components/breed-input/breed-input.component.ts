import { Component, OnInit, ElementRef, AfterViewInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { SINGLE_BREEDS, MIXED_BREEDS } from './breed.data';
import { BreedSearchPipe } from 'src/app/pipes/breed-search.pipe';

export interface BreedOption {
  type?: 'single' | 'mixed' | 'special' | 'custom';
  name?: string;
  id?: string;
}

const UNKNOWN_BREED: BreedOption = {
  type: 'special',
  name: 'Unknown',
  id: 'unknown'
};

const OTHER_BREED: BreedOption = {
  type: 'special',
  name: 'Other',
  id: 'other'
};

@Component({
  selector: 'app-breed-input',
  templateUrl: './breed-input.component.html',
  styleUrls: ['./breed-input.component.scss']
})
export class BreedInputComponent implements OnInit, AfterViewInit {

  breedOptions: BreedOption[] = [];
  specialBreedOptions: BreedOption[] = [];

  breedMatches: BreedOption[] = [];
  hasNoBreedMatches = false;

  @Input('selectedBreed') selectedBreed: BreedOption;

  form: FormGroup;
  input: HTMLFormElement;

  searchPipe = new BreedSearchPipe();

  isDropdownOpen = false;
  activeDropdownOption = -1;

  constructor(
    private fb: FormBuilder,
    private element: ElementRef
  ) {
    this.prepareBreedOptions();

    this.form = fb.group({
      breedName: ['', [Validators.required]],
    });

    this.form.controls['breedName'].valueChanges.subscribe(breedName => {
      this.findBreedMatches(breedName);
      this.activeDropdownOption = -1;
      this.isDropdownOpen = true;
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.input = this.element.nativeElement.querySelector('input') as HTMLFormElement;

    this.input.addEventListener('focus', ev => {
      this.isDropdownOpen = true;
    });

    this.input.addEventListener('blur', ev => {
      setTimeout(() => {
        this.isDropdownOpen = false;
      }, 100);
    });

    this.input.addEventListener('keydown', ev => {
      switch (ev.key) {
        case 'ArrowDown':
          this.activeDropdownOption++;
          if (this.activeDropdownOption >= this.breedMatches.length) {
            this.activeDropdownOption = this.breedMatches.length - 1;
          }
          break;
        case 'ArrowUp':
          this.activeDropdownOption--;
          if (this.activeDropdownOption < 0 ) {
            this.activeDropdownOption = 0;
          }
          break;
        case 'Enter':
          if (this.breedMatches[this.activeDropdownOption]){
            this.selectBreedMatch(this.breedMatches[this.activeDropdownOption]);
          } else if (this.hasNoBreedMatches) {
            const customEntry: BreedOption = {
              name: this.form.value.breedName,
              id: getBreedIdFromName(this.form.value.breedName),
              type: 'custom'
            };

            this.selectBreedMatch(customEntry);
          }
          break;
      }
    });
  }

  prepareBreedOptions() {
    for (const breed of SINGLE_BREEDS) {
      this.breedOptions.push({
        type: 'single',
        name: breed,
        id: getBreedIdFromName(breed)
      });
    }

    for (const breed of MIXED_BREEDS) {
      this.breedOptions.push({
        type: 'mixed',
        name: breed,
        id: getBreedIdFromName(breed)
      });
    }

    this.breedOptions = this.breedOptions.sort((a, b) => a.name.localeCompare(b.name));
    this.breedOptions.unshift(UNKNOWN_BREED);

    this.breedMatches = this.breedOptions;
  }

  findBreedMatches(searchTerm: string) {
    if (!searchTerm) {
      this.breedMatches = this.breedOptions;
      return;
    }

    this.breedMatches = this.searchPipe.transform(this.breedOptions, searchTerm);

    if (!this.breedMatches.length) {
      this.breedMatches.push(OTHER_BREED);
      this.hasNoBreedMatches = true;
    } else {
      this.hasNoBreedMatches = false;
    }
  }

  selectBreedMatch(breedOption: BreedOption) {
    Object.assign(this.selectedBreed, breedOption);
    this.form.controls['breedName'].setValue(this.selectedBreed.name);
    this.input.blur();
  }

  openDropdown() {
    this.activeDropdownOption = -1;
    this.isDropdownOpen = true;
  }


}

function getBreedIdFromName(name: string) {
  return name.toLowerCase().replace(/\s+/g, '-');
}
