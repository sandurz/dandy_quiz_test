import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreedInputComponent } from './breed-input.component';

describe('BreedInputComponent', () => {
  let component: BreedInputComponent;
  let fixture: ComponentFixture<BreedInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreedInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
