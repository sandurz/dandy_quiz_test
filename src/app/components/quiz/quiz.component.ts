import { Component, OnInit } from '@angular/core';

import { BreedOption } from '../breed-input/breed-input.component';

enum QuizStep {
  'Breed',
  'BreedConfirm'
}

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  selectedBreeds: BreedOption[] = [{}];

  maxBreeds = 3;

  currentStep = QuizStep.Breed;

  constructor() {
  }

  ngOnInit() {
  }
  
  addBreed() {
    if (this.selectedBreeds.length < this.maxBreeds) {
      this.selectedBreeds.push({} as BreedOption);
    }
  }

  checkBreedsChosen() {
    for (const selection of this.selectedBreeds) {
      if (!selection.id) {
        return false;
      }
    }

    return true;
  }

  next() {
    this.currentStep++;
  }
}


